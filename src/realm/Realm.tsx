import { isEmpty } from "lodash";
import Realm from "realm";
import { TaskSchema } from "./Schema";

export default class RealmObject {
    private static _instance;
    private realmObj;
    constructor() {
        if (RealmObject._instance) {
            return RealmObject._instance
        }
        RealmObject._instance = this;

    }


    async initRealm() {
        if (!this.realmObj) {
            this.realmObj = await Realm.open({
                path: "TodoRealm",
                schema: [TaskSchema],
            });
        }
    }

    getAllTask() {
        const taskObj = this.realmObj.objects('Task').sorted('priority', false);
        return taskObj
    }

    createTask(item) {
        let task;
        const taskObj = this.realmObj.objects('Task').sorted('_id', true);
        this.realmObj.write(() => {
            task = this.realmObj.create("Task", {
                _id: (taskObj.length > 0) ? taskObj[0]._id + 1: 0,
                content: item.content,
                date: item.date,
                priority: item.priority
            });
        });

        return task;
    }

    updateTask(id, item) {
        let task = this.realmObj.objects('Task').filtered(`_id = ${id}`)[0];
        this.realmObj.write(() => {
            task.content = item.content;
            task.date = item.date;
            task.priority = item.priority;
        });

        return task;
    }

    deleteTasks(arrId = []) {
        if (isEmpty(arrId)) return;
        let taskList = [];
        for (let index in arrId) {
            let task = this.realmObj.objects('Task').filtered(`_id = ${arrId[index]}`)[0];
            taskList.push(task);
        }

        this.realmObj.write(() => {
            for (let index in taskList) {
                this.realmObj.delete(taskList[index])
            }
        });
    }
}