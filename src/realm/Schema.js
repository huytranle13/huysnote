export const TaskSchema = {
    name: "Task",
    properties: {
        _id: "int",
        content: "string",
        date: "string",
        priority: "int"
    },
    primaryKey: "_id",
};

