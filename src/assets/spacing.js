export default class Spacing {
    static XXS = 2;
    static XS = 4;
    static S = 8;
    static M = 12;
    static L = 16;
    static XL = 24;
    static XXL = 32;
}