export default class Colors {
    static primary = "#F7CC15";
    static background = "#EFF2FE";
    static primaryTransparent = "rgba(55, 211, 200, 0.15)";
    static text = "#2C2C2C";
    static gray = "#999999";
    static gray_opacity = "#99999900";
    static light_gray = "#F6F6F6";
    static medium_gray = "#666666";
    static border = "#C4C4C4";
    static black = "#000";
    static black_theme = '#3d3d3d';
    static facebook = "#0C74D5";
    static google = "#F53A27";
    static apple = "#000000";
    static mainButton = "#F65D79";
    static white = "#fff";
    static tabBarIcon = "#7C7C7C";
    static red = "#ed1c24";
    static green = "#18ac2c";
    static orange = '#F7B733';
    static link='#2200cc';
}