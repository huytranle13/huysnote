import moment from 'moment';
import React from 'react';
import { Animated, Dimensions, Easing, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters'
import DropdownAlert from "react-native-dropdownalert";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Modal from 'react-native-modalbox';
import Colors from './assets/color';
import { Img } from './assets/images';
import Spacing from './assets/spacing';
import Input from './components/Input';
import ItemList from './components/ItemList';
import Picker from './components/Picker';
import { isEmpty } from 'lodash';
import RealmObject from './realm/Realm';

const { width } = Dimensions.get('screen')

export default class MainApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            startCreate: false,
            createHeight: new Animated.Value(0),
            createOpacity: new Animated.Value(0),
            content: '',
            priority: 1,
            date: '',
            isDatePickerVisible: false,
            selectItem: [],
            createContentHeight: 0,
        }
        this.dropdown = React.createRef();
        this.realm = new RealmObject();
    }

    async componentDidMount() {
        await this.realm.initRealm()
        this.getAllTask()
    }

    animateCreateForm(to, callback) {
        this.animation = Animated.parallel([
            Animated.timing(this.state.createOpacity, {
                toValue: to,
                duration: 1000,
                useNativeDriver: false,
                delay: 0
            }),
            Animated.timing(this.state.createHeight, {
                toValue: to,
                duration: 800,
                useNativeDriver: false,
                delay: 0
            })
        ], { stopTogether: false }).start(() => {
            if (callback) callback()

        });
    }

    getAllTask() {
        const list = this.realm.getAllTask();
        this.setState({
            list
        })
    }

    deleteItem(id) {
        this.realm.deleteTasks([id])
        this.getAllTask();
    }

    submit() {
        const { content, priority, date } = this.state;
        if (isEmpty(content)) {
            this.dropdown.alertWithType('error', "Thiếu nội dung nhiệm vụ", "Vui lòng nhập nội dung nhiệm vụ để tiếp tục")
            return
        }
        if (isEmpty(date)) {
            this.dropdown.alertWithType('error', "Thiếu thời gian", "Vui lòng nhập thời gian để tiếp tục")
            return
        }
        this.realm.createTask({
            content, date, priority
        });
        this.getAllTask();

        this.animateCreateForm(0, () => {
            this.setState({
                startCreate: false,
            });
        })


    }

    editItem(data) {
        const { content, priority, date, _id } = data;
        if (isEmpty(content)) {
            this.dropdown.alertWithType('error', "Thiếu nội dung nhiệm vụ", "Vui lòng nhập nội dung nhiệm vụ để tiếp tục")
            return
        }
        if (isEmpty(date)) {
            this.dropdown.alertWithType('error', "Thiếu thời gian", "Vui lòng nhập thời gian để tiếp tục")
            return
        }

        this.realm.updateTask(_id, {
            content, date, priority
        });
        this.getAllTask();
    }

    deleteMultipleItem() {
        const { selectItem } = this.state;
        this.realm.deleteTasks(selectItem)
        this.setState({
            selectItem: []
        })
        this.getAllTask();
    }

    addToSelectItem(value, item) {
        const { selectItem } = this.state;
        if (value) {
            selectItem.push(item._id);
        } else {
            const id = selectItem.findIndex((it, index) => it == item._id);
            if (id >= 0) {
                selectItem.splice(id, 1);
            }
        }
        this.setState({
            selectItem
        })
    }

    render() {
        const { list, startCreate, isDatePickerVisible, date, selectItem, createHeight, createContentHeight, createOpacity } = this.state
        const maxHeight = createHeight.interpolate({
            inputRange: [0, 1],
            outputRange: [0, verticalScale(260)]
        });
        return (
            <SafeAreaView style={styles.main}>
                <View style={styles.mainHeader}>
                    <Text style={styles.headerText}>To-do List</Text>
                    {(isEmpty(selectItem)) ? null : (
                        <TouchableOpacity style={styles.deleteMultiple} onPress={() => {
                            this.deleteMultipleItem()
                        }}>
                            <Image source={Img.remove} resizeMode={'contain'} style={styles.deleteImg} />
                        </TouchableOpacity>
                    )}

                </View>
                {(startCreate) ? (
                    <Animated.View style={[styles.create, { height: maxHeight, opacity: createOpacity }]}>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={() => {
                                this.animateCreateForm(0, () => {
                                    this.setState({
                                        startCreate: false,
                                        content: '',
                                        priority: 1,
                                        date: ''
                                    })
                                });
                            }} style={styles.deletebtn} >
                                <Image source={Img.delete} resizeMode={'contain'} style={styles.deleteImg} />
                                <Text style={styles.deleteText}>Xoá</Text>
                            </TouchableOpacity>

                            <Input value={this.state.content}
                                placeholder={'Nội dung việc cần làm'}
                                onChangeText={(value) => {
                                    this.setState({ content: value });
                                }} />

                            <TouchableOpacity style={styles.inputField} onPress={() => {
                                this.setState({
                                    isDatePickerVisible: true
                                })
                            }}>
                                <Text style={styles.inputFieldText}>Thời hạn</Text>
                                <Text style={styles.inputResult}>{(isEmpty(date)) ? 'Hãy chọn thời gian' : date}</Text>
                            </TouchableOpacity>

                            <Picker
                                placeholder={'Độ ưu tiên'}
                                defaultValue={this.state.priority}
                                options={[
                                    { label: 'Cao', value: 0 },
                                    { label: 'Trung bình', value: 1 },
                                    { label: 'Thấp', value: 2 }
                                ]}
                                valueChange={(value) => {
                                    this.setState({ priority: value });
                                }}
                            // error={(this.state.dayError) ? this.state.dayError[0] : null}
                            />
                        </View>


                        <TouchableOpacity style={styles.btnSubmit} onPress={() => {
                            this.submit();

                        }}>
                            <Text style={styles.textSubmit}>Xong</Text>
                        </TouchableOpacity>
                    </Animated.View>
                ) : null}
                <View style={styles.listTodo}>
                    <FlatList
                        data={list}
                        keyExtractor={(item) => item._id}
                        renderItem={({ item }) => (
                            <ItemList data={item} onCheck={(value, item) => this.addToSelectItem(value, item)}
                                onEditItem={(data) => {
                                    this.editItem(data)
                                }} onDeleteItem={(id) => {
                                    this.deleteItem(id)
                                }} />
                        )}
                    />
                </View>

                <TouchableOpacity style={styles.createBtn} onPress={() => {
                    this.setState({
                        startCreate: true,
                        content: '',
                        priority: 1,
                        date: ''
                    }, () => {
                        this.animateCreateForm(1)
                    });

                }}>
                    <Text style={styles.createText}>Tạo Task mới +</Text>
                </TouchableOpacity>
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="datetime"
                    minimumDate={moment().add(1, 'hour').toDate()}
                    onConfirm={(date) => {
                        this.setState({
                            isDatePickerVisible: false,
                            date: moment(date).format('HH:mm DD-MM-YYYY')
                        })
                    }}
                    onCancel={() => {
                        this.setState({
                            isDatePickerVisible: false
                        })
                    }}
                />
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000} updateStatusBar={false}
                    titleStyle={{
                        paddingTop: verticalScale(Spacing.XL), fontSize: 16,
                        textAlign: 'left',
                        fontWeight: 'bold',
                        color: Colors.white,
                        backgroundColor: 'transparent'
                    }}
                    imageStyle={{
                        marginTop: verticalScale(Spacing.XL),
                        paddingHorizontal: 8,
                        width: 36,
                        height: 36,
                        alignSelf: 'center'
                    }}
                />


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: Colors.primary,
        height: '100%'
    },
    mainHeader: {
        width: '100%',
        paddingVertical: verticalScale(Spacing.M),
        paddingHorizontal: moderateScale(Spacing.L),
        alignItems: 'center'
    },
    deleteMultiple: {
        position: 'absolute',
        right: scale(Spacing.L),
        top: verticalScale(Spacing.M), bottom: verticalScale(Spacing.M)
    },
    headerText: {
        color: Colors.white,
        fontSize: scale(Spacing.L),
        fontWeight: 'bold'
    },
    listTodo: {
        flex: 1, justifyContent: "center", width: "100%",
        paddingBottom: verticalScale(Spacing.L),
        marginTop: verticalScale(Spacing.L)
    },
    createBtn: {
        borderRadius: scale(50 / 2),
        backgroundColor: Colors.mainButton,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: moderateScale(Spacing.L),
        marginBottom: verticalScale(Spacing.M)
    },
    createText: {
        color: Colors.white,
        fontSize: scale(Spacing.L),
    },
    create: {
        backgroundColor: Colors.white,
        borderRadius: scale(Spacing.M),
        // marginVertical: verticalScale(Spacing.M),
        marginHorizontal: moderateScale(Spacing.L),
        paddingHorizontal: moderateScale(Spacing.L),
        overflow: "hidden"
    },
    deleteImg: {
        width: scale(15),
        height: scale(15),
        padding: scale(Spacing.S),
    },
    deletebtn: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        marginTop: verticalScale(Spacing.L)
    },
    deleteText: {
        color: Colors.black,
        fontSize: scale(Spacing.M),
        marginLeft: moderateScale(Spacing.S)
    },
    btnSubmit: {
        backgroundColor: Colors.green,
        borderRadius: scale(15),
        height: scale(30),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: moderateScale(Spacing.XL),
        marginVertical: verticalScale(Spacing.M),
    },
    textSubmit: {
        color: Colors.white,
        fontSize: scale(Spacing.M),
        fontWeight: '600'
    },
    inputField: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Colors.black,
        marginTop: verticalScale(Spacing.M),
        paddingVertical: verticalScale(Spacing.S),
        justifyContent: 'space-between'
    },
    inputFieldText: {
        color: Colors.black,
        fontSize: scale(Spacing.L),
        fontWeight: '600'
    },
    inputResult: {
        color: Colors.gray,
        fontSize: scale(Spacing.L),
    },
    viewModal: {
        height: scale(500),
        borderTopRightRadius: scale(Spacing.L),
        borderTopLeftRadius: scale(Spacing.L),
    },
})