import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import Colors from 'src/assets/color';
import Spacing from 'src/assets/spacing';

const Input = (props) => {
    return (
        <View style={{}}>
            <View>
                <TextInput
                    style={[styles.input, props.inputStyle, { backgroundColor: Colors.white }, (props.error) ? styles.errorBorder : null]}
                    {...props}
                    
                    // onChangeText={onChangeNumber}
                    // value={number}
                    
                    placeholderTextColor={Colors.gray}
                    placeholder={props.placeholder}
                    underlineColorAndroid="transparent"
                />

                {(props.rightComponent) ? (
                    <View style={styles.rightComponent}>
                        {props.rightComponent}
                    </View>
                ) : null}
            </View>


            {(props.error) ? (
                <Text style={styles.errorText}>{props.error}</Text>
            ) : null}
        </View>

    );
}

const styles = StyleSheet.create({
    input: {
        padding: scale(Spacing.S),
        backgroundColor: Colors.light_gray,
        borderRadius: scale(Spacing.XS),
        fontSize: scale(Spacing.L),
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: Colors.black
    },
    rightComponent: {
        position: 'absolute',
        right: 0,
        top: 0, bottom: 0,
        alignItems: 'center', justifyContent: 'center'
    },
    errorText: {
        color: Colors.red,
        fontSize: scale(Spacing.M),
        marginTop: verticalScale(Spacing.S)
    },
    errorBorder: {
        borderWidth: 1,
        borderColor: Colors.red
    }
});

export default Input;