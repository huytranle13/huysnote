import React, { useEffect, useRef, useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from "react-native-modalbox";
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import Spacing from 'src/assets/spacing';
import Colors from 'src/assets/color';

const Picker = (props) => {

    const [value, setValue] = useState(null);
    const { defaultValue, showIndicator = false, changePlaceholder= true, options= [] } = props;
    var modal = useRef();

    useEffect(() => {
        // Your code here
        // if (props.defaultValue != undefined) {
            setValue(defaultValue);
        // }
    }, [defaultValue]);

    const renderItem = (index, label, iValue) => {
        return (
            <TouchableOpacity key={index} style={styles.item} onPress={() => {
                setValue(iValue);
                props.valueChange(iValue);
                modal.current.close();
            }}>
                <Text style={[styles.itemText, (iValue === value) ? { color: Colors.buttonOrange } : null]}>{label}</Text>
            </TouchableOpacity>
        );
    }

    return (
        <View style={{  }}>
            <TouchableOpacity style={[styles.input, { backgroundColor: Colors.white }, (props.error) ? styles.errorBorder : null]} onPress={() => {
                modal.current.open();
            }}>
                <Text style={styles.txtInput}>{ props.placeholder}</Text>
                <Text style={styles.inputResult}>{(value !=null) ? (options.find((item) => item.value == value)).label: ''}</Text>
                
            </TouchableOpacity>
            {(props.error) ? (
                <Text style={styles.errorText}>{props.error}</Text>
            ) : null}
            <Modal position={"bottom"}
                coverScreen={true}
                useNativeDriver
                ref={modal}
                style={styles.viewModal}>
                <View style={[styles.contentBlock, { backgroundColor: Colors.white }]}>
                    <View style={styles.head}>
                        <Text style={styles.headText}>{props.placeholder}</Text>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {(props.options && typeof props.options === 'object') ? props.options.map((item, index) => {
                            return renderItem(index, item.label, item.value);
                        }) : null}
                    </ScrollView>
                </View>

            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Colors.black,
        marginTop: verticalScale(Spacing.M),
        paddingVertical: verticalScale(Spacing.S),
        justifyContent: 'space-between'
    },
    txtInput: {
        color: Colors.black,
        fontSize: scale(Spacing.L),
        fontWeight: '600',
        flex: 1
    },
    inputResult: {
        color: Colors.gray,
        fontSize: scale(Spacing.L),
    },
    head: {
        padding: scale(Spacing.L),
        alignItems: 'center',
    },
    headText: {
        fontSize: scale(Spacing.XL),
    },
    viewModal: {
        height: verticalScale(500),
        borderTopRightRadius: scale(Spacing.L),
        borderTopLeftRadius: scale(Spacing.L),
    },
    contentBlock: {
        backgroundColor: Colors.white,
        height: scale(500),
        borderTopRightRadius: scale(Spacing.L),
        borderTopLeftRadius: scale(Spacing.L)
    },
    item: {
        flexDirection: 'row',
        padding: scale(Spacing.L)
    },
    itemText: {
        fontSize: scale(Spacing.L),
        flex: 1
    },
    errorText: {
        color: Colors.red,
        fontSize: scale(Spacing.M),
        marginTop: verticalScale(Spacing.S)
    },
    errorBorder: {
        borderWidth: 1,
        borderColor: Colors.red
    }
});

export default Picker;