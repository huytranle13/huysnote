import CheckBox from '@react-native-community/checkbox';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react'
import { Animated, Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import TimeAgo from 'react-native-timeago';
import Colors from '../assets/color';
import { Img } from '../assets/images';
import Spacing from '../assets/spacing';
import Input from './Input';
import Picker from './Picker';
require('moment/locale/vi');
moment.locale('vi');

const ItemList = (props) => {
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    const [toggleEdit, setToggleEdit] = useState(false)
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false)
    const [contentTask, setContentTask] = useState('');
    const [dateTask, setDateTask] = useState(null);
    const [priorityTask, setPriorityTask] = useState(1);
    const [remainSec, setRemainSec] = useState(0);
    const [editHeight, setEditHeight] = useState(0);
    const [animatedHeight, setAnimatedHeight] = useState(new Animated.Value(500));
    const [opacityList, setOpacityList] = useState(new Animated.Value(1));

    var animatedX = new Animated.Value(0)
    var animatedY = new Animated.Value(0)


    const { data: { content, priority, date, _id }, onCheck, onEditItem, onDeleteItem } = props

    const mapPriority = () => {
        switch (priority) {
            case 0: return { color: Colors.green, text: 'cao' }
            case 1: return { color: Colors.orange, text: 'trung bình' }
            case 2: return { color: Colors.black, text: 'thấp' }
        }
    }

    useEffect(() => {
        setContentTask(content);
        setDateTask(date);
        setPriorityTask(priority);
        calculateRemaining();
        let myInterval = setInterval(calculateRemaining, 1000 * 60)
        return () => {
            clearInterval(myInterval);
        };
    }, [props.data])

    const calculateRemaining = () => {
        if (moment().isBefore(moment(date, 'HH:mm DD-MM-YYYY'))) {
            const second = moment(date, 'HH:mm DD-MM-YYYY').diff(moment(), 'seconds')
            // console.log(second)
            setRemainSec(second);
        } else {
            setRemainSec(0)
        }
    }

    const calculateRemainingText = () => {
        if (remainSec > 86400) {
            const days = Math.floor(remainSec / 86400);
            return `Còn ${days} ngày`
        }
        if (remainSec > 3600) {
            const hours = Math.floor(remainSec / 3600);
            return `Còn ${hours} giờ`
        }
        if (remainSec > 60) {
            const minutes = Math.floor(remainSec / 60);
            return `Còn ${minutes} phút`
        }
        return `Còn ${remainSec} giây`
    }

    const animateEditForm = (toHeight, toListOp, toX, toY, callback) => {
        console.log(toX, toY)
        Animated.parallel([
            Animated.timing(animatedHeight, {
                toValue: toHeight,
                duration: 1000,
                useNativeDriver: false,
            }),
            Animated.timing(opacityList, {
                toValue: toListOp,
                duration: 1200,
                useNativeDriver: false,
            }),
            Animated.timing(animatedX, {
                toValue: toX,
                duration: 600,
                useNativeDriver: false,
            }),
            Animated.timing(animatedY, {
                toValue: toY,
                duration: 600,
                useNativeDriver: false,
            })
        ], { stopTogether: false }).start(() => {
            if (callback) callback()
        });
    }

    return (
        <Animated.View style={[{
            marginBottom: verticalScale(Spacing.L),
            height: animatedHeight
        }, styles.edit]}>
            <TouchableWithoutFeedback onPress={() => {
                animateEditForm(editHeight + verticalScale(180) , 0, -20, 30, () => {
                    setToggleEdit(true);
                })

            }}
                pointerEvents={'box-none'}>

                <View onLayout={(event) => {
                    var { x, y, width, height } = event.nativeEvent.layout;
                    if (editHeight == 0) {
                        setEditHeight(height + verticalScale(Spacing.L) * 2)
                        setAnimatedHeight(new Animated.Value(height + verticalScale(Spacing.L) * 2))
                    }

                }}>
                    {(toggleEdit) ? (
                        <View>
                            <TouchableOpacity onPress={() => {
                                onDeleteItem(_id);
                                setToggleEdit(false);
                            }} style={styles.deletebtn}>
                                <Image source={Img.delete} resizeMode={'contain'} style={styles.deleteImg} />
                                <Text style={styles.deleteText}>Xoá</Text>
                            </TouchableOpacity>

                            <Input value={contentTask}
                                placeholder={'Nội dung việc cần làm'}
                                onChangeText={(value) => {
                                    setContentTask(value)
                                }} />

                            <TouchableOpacity style={styles.inputField} onPress={() => {
                                setIsDatePickerVisible(true)
                            }}>
                                <Text style={styles.inputFieldText}>Thời hạn</Text>
                                <Text style={styles.inputResult}>{(isEmpty(dateTask)) ? 'Hãy chọn thời gian' : dateTask}</Text>
                            </TouchableOpacity>

                            <Picker
                                placeholder={'Độ ưu tiên'}
                                defaultValue={priorityTask}
                                options={[
                                    { label: 'Cao', value: 0 },
                                    { label: 'Trung bình', value: 1 },
                                    { label: 'Thấp', value: 2 }
                                ]}
                                valueChange={(value) => {
                                    setPriorityTask(value)
                                }}
                            />

                            <TouchableOpacity style={styles.btnSubmit} onPress={() => {
                                onEditItem({
                                    _id,
                                    content: contentTask,
                                    date: dateTask,
                                    priority: priorityTask
                                });
                                setToggleEdit(false);
                                animateEditForm(editHeight, 1, 0, 0)

                            }}>
                                <Text style={styles.textSubmit}>Xong</Text>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <Animated.View style={[{
                            opacity: opacityList
                        }, styles.list]} >
                            <TouchableWithoutFeedback>
                                <CheckBox
                                    disabled={false}
                                    value={toggleCheckBox}
                                    boxType={'square'}
                                    style={{ width: 20, height: 20, padding: 2 }}
                                    tintColors={{ true: Colors.green, false: Colors.gray }}
                                    tintColor={Colors.gray}
                                    onCheckColor={Colors.white}
                                    onFillColor={Colors.green}
                                    onTintColor={Colors.green}
                                    onValueChange={(newValue) => {
                                        setToggleCheckBox(newValue)
                                        onCheck(newValue, props.data);

                                    }}
                                />
                            </TouchableWithoutFeedback>

                            <View style={styles.contentList}>
                                <Animated.View style={{
                                    transform: [
                                        { translateX: animatedX },
                                        { translateY: animatedY },
                                    ]
                                }}>
                                    <Text style={styles.contentText} >{content}</Text>
                                </Animated.View>

                                <Text style={[{ color: mapPriority().color }, styles.priorityText]}>Ưu tiên {mapPriority().text}</Text>
                            </View>


                            <View style={styles.tool}>
                                <TouchableOpacity onPress={() => { }}>
                                    <Image source={Img.edit} style={styles.editBtn} />
                                </TouchableOpacity>

                                {(remainSec > 0) ? (
                                    <Text style={styles.time}>
                                        {calculateRemainingText()}
                                    </Text>
                                ) : (
                                    <Text style={styles.time}>Tới hạn <TimeAgo time={moment(date, 'HH:mm DD-MM-YYYY')} /></Text>
                                )}

                            </View>
                        </Animated.View>
                    )}


                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="datetime"
                        minimumDate={moment().add(1, 'hour').toDate()}
                        onConfirm={(date) => {
                            setIsDatePickerVisible(false);
                            setDateTask(moment(date).format('HH:mm DD-MM-YYYY'))
                        }}
                        onCancel={() => {
                            setIsDatePickerVisible(false);
                        }}
                    />
                </View>


            </TouchableWithoutFeedback >
        </Animated.View>
    );
}

export default ItemList;

const styles = StyleSheet.create({
    list: {

        flexDirection: 'row',

    },
    editAnimated: {
        marginTop: verticalScale(Spacing.L),
        marginHorizontal: moderateScale(Spacing.L),
        overflow: 'hidden',
    },
    edit: {
        paddingHorizontal: moderateScale(Spacing.L),
        paddingVertical: verticalScale(Spacing.L),
        backgroundColor: Colors.white,
        borderRadius: scale(Spacing.M),
        marginHorizontal: moderateScale(Spacing.L),
        overflow: 'hidden',
    },
    deleteImg: {
        width: scale(15),
        height: scale(15),
        padding: scale(Spacing.S),
    },
    deletebtn: {
        alignSelf: 'flex-end',
        flexDirection: 'row'
    },
    deleteText: {
        color: Colors.black,
        fontSize: scale(Spacing.M),
        marginLeft: moderateScale(Spacing.S)
    },
    contentList: {
        flex: 1,
        paddingHorizontal: moderateScale(Spacing.L)
    },
    contentText: {
        color: Colors.black,
        fontSize: scale(Spacing.M),
        fontWeight: '800'
    },
    priorityText: {
        fontSize: scale(Spacing.M),
        marginTop: verticalScale(Spacing.L)
    },
    tool: {
        alignItems: 'flex-end',
        justifyContent: 'space-between'
    },
    editBtn: {
        width: scale(30),
        height: scale(30),
        padding: scale(Spacing.S)
    },
    time: {
        fontSize: scale(Spacing.M),
        color: Colors.black,
    },
    btnSubmit: {
        backgroundColor: Colors.green,
        borderRadius: scale(15),
        height: scale(30),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: moderateScale(Spacing.XL),
        marginTop: verticalScale(Spacing.L)
    },
    textSubmit: {
        color: Colors.white,
        fontSize: scale(Spacing.M),
        fontWeight: '600'
    },
    inputField: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: Colors.black,
        marginTop: verticalScale(Spacing.M),
        paddingVertical: verticalScale(Spacing.S),
        justifyContent: 'space-between'
    },
    inputFieldText: {
        color: Colors.black,
        fontSize: scale(Spacing.L),
        fontWeight: '600'
    },
    inputResult: {
        color: Colors.gray,
        fontSize: scale(Spacing.L),
    },
    viewModal: {
        height: scale(500),
        borderTopRightRadius: scale(Spacing.L),
        borderTopLeftRadius: scale(Spacing.L),
    },
})